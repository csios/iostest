import UIKit
import RxSwift
import RxCocoa

class DashboardViewController: UIViewController {

    @IBOutlet weak var scoreProgressView: CircularProgressView!
    @IBOutlet weak var updateDateLabel: UILabel!
    @IBOutlet weak var firstLoadingLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    
    @IBOutlet weak var firstLoadingIndicator: UIActivityIndicatorView! {
        didSet {
            firstLoadingIndicator.hidesWhenStopped = true
        }
    }

    @IBOutlet weak var refreshLoadingIndicator: UIActivityIndicatorView! {
        didSet {
            refreshLoadingIndicator.hidesWhenStopped = true
        }
    }
    
    let disposeBag = DisposeBag()
    var viewModel: DashboardViewModel?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        styleNavigationBar()
        
        bind()
    }
    
    @IBAction func refreshCreditScore(_ sender: UIButton) {
        viewModel?.refreshCreditScore()
    }
    
    func styleNavigationBar() {
        navigationItem.title = "Dashboard"
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func bind() {
        guard let viewModel = viewModel else { return }
        
        viewModel.viewState.asObservable().subscribe(onNext: process(state:))
            .addDisposableTo(disposeBag)
        
        viewModel.updatedAt.asObservable().bindTo(updateDateLabel.rx.text)
            .addDisposableTo(disposeBag)
        
        Observable.combineLatest(viewModel.creditScore.asObservable(), viewModel.maxScore.asObservable()) { ($0, $1) }
            .subscribe(onNext: refreshProgress(scoreInfo:))
            .addDisposableTo(disposeBag)
    }

    func refreshProgress(scoreInfo: (creditScore: Int, maxScore: Int)) {
        scoreProgressView.maxScore = scoreInfo.maxScore
        scoreProgressView.animate(score: scoreInfo.creditScore)
    }
    
    func process(state: DashboardState) {
        switch state {
        case .noData:
            firstLoadingLabel.isHidden = false
            firstLoadingIndicator.startAnimating()
            scoreProgressView.isHidden = true
            updateDateLabel.isHidden = true
            refreshButton.isHidden = true
            refreshLoadingIndicator.stopAnimating()
            
            viewModel?.refreshCreditScore()
                        
        case .loaded:
            firstLoadingLabel.isHidden = true
            firstLoadingIndicator.stopAnimating()
            scoreProgressView.isHidden = false
            updateDateLabel.isHidden = false
            refreshButton.isHidden = false
            refreshLoadingIndicator.stopAnimating()
            
        case .loadError(let error):
            process(error: error, refreshing: false)
            
        case .refreshError(let error):
            refreshButton.isHidden = false
            refreshLoadingIndicator.stopAnimating()
            process(error: error, refreshing: true)
            
        case .refreshing:
            firstLoadingLabel.isHidden = true
            firstLoadingIndicator.stopAnimating()
            scoreProgressView.isHidden = false
            updateDateLabel.isHidden = false
            refreshButton.isHidden = true
            refreshLoadingIndicator.startAnimating()
        }
    }
    
    func process(error: String, refreshing: Bool) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        
        if refreshing {
            let ok = UIAlertAction(title: "OK", style: .default)
            alert.addAction(ok)
        } else {
            let retry = UIAlertAction(title: "Retry", style: .default, handler: { action in
                self.viewModel?.refreshCreditScore()
            })
            
            alert.addAction(retry)
        }
        
        present(alert, animated: true)
    }
}
