import UIKit

class GradientView: UIView {
    
    var configuration = SegmentedGradientConfiguration()
    
    var progress: Double = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        let endAngle = Int((Double(configuration.maxAngle) * progress))
        
        guard endAngle > 0 else { return }
        
        let center = CGPoint(x: rect.midX, y: rect.midY)
        let radius = rect.width / 2 - (configuration.lineWidth / 2)
        let path = UIBezierPath()
        
        (0..<endAngle).forEach { angle in
            let arc = drawArc(angle: angle, center: center, radius: radius)
            path.append(arc)
        }
    
        path.close()
    }
    
    // Draw the individual segments, each one with a slightly different color, creating the gradient effect
    private func drawArc(angle: Int, center: CGPoint, radius: CGFloat) -> UIBezierPath {
        let color = configuration.determineColor(at: angle)
        color.setStroke()
        
        let point = UIBezierPath(arcCenter: center, radius: radius, startAngle: (angle + configuration.initialAngle).toRadians, endAngle: ((angle + 1) + configuration.initialAngle).toRadians, clockwise: true)
        point.lineWidth = configuration.lineWidth
        point.stroke()

        return point
    }
    
}
