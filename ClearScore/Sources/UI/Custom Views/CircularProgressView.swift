import UIKit

@IBDesignable class CircularProgressView: UIView {
    
    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var maxScoreLabel: UILabel!
    
    var contentView : UIView!
    var timer: Timer?
    var currentPercentage: Double = 0
    var percentageToAnimate: Double = 0
    
    var maxScore = 0 {
        didSet {
            maxScoreLabel.text = "out of \(maxScore)"
        }
    }
    
    var progress: Double = 0 {
        didSet {
            gradientView.progress = progress
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
        configure()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        contentView.backgroundColor = .clear
        
        addSubview(contentView)
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: Constants.Xibs.circularProgressView, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: .none).first as! UIView
        
        return view
    }
    
    func configure() {
        layer.borderColor = UIColor.darkGray.withAlphaComponent(0.8).cgColor
        layer.borderWidth = 2
        layer.cornerRadius = bounds.width / 2
    }
    
    func animate(score: Int) {
        guard score >= 0, maxScore > 0 else { return }
        
        if let timer = timer {
            timer.invalidate()
        }
        
        scoreLabel.text = "\(score)"
        let angle = Int(score * (gradientView.configuration.maxAngle - 1) / maxScore)
        scoreLabel.textColor = gradientView.configuration.determineColor(at: angle)
        
        guard score > 0 else {
            progress = 0
            return
        }

        // Animate circle
        currentPercentage = 0.0
        percentageToAnimate = Double(score) / Double(maxScore)
        
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateProgressAtRatio), userInfo: .none, repeats: true)
        RunLoop.main.add(timer!, forMode: RunLoopMode.commonModes)
    }
    
    func updateProgressAtRatio() {
        guard let timer = timer else { return }

        currentPercentage += 0.01
        progress = currentPercentage
        
        if currentPercentage > percentageToAnimate {
            timer.invalidate()
        }
    }
    
}
