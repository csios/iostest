import Foundation
import SwiftyJSON

struct CreditReport {
    let score: Int
    let minScoreValue: Int
    let maxScoreValue: Int
    let updatedAt: Date
}

extension CreditReport: JSONAble {
    
    init?(json: JSON) {
        guard
            let info = json["creditReportInfo"].dictionary,
            let score = info["score"]?.int,
            let minScoreValue = info["minScoreValue"]?.int,
            let maxScoreValue = info["maxScoreValue"]?.int
        else { return nil }
        
        self.score = score
        self.minScoreValue = minScoreValue
        self.maxScoreValue = maxScoreValue
        self.updatedAt = Date()
    }
}

extension CreditReport: Serializable {
    
    func asDictionary() -> [String : Any] {
        var dict: [String : Any] = [:]
        
        dict = [
            "score": score,
            "minScoreValue": minScoreValue,
            "maxScoreValue": maxScoreValue,
            "updatedAt": updatedAt
        ]
        
        return dict
    }
    
    init?(with dictionary: [String : Any]) {
        guard let score = dictionary["score"] as? Int,
            let minScoreValue = dictionary["minScoreValue"] as? Int,
            let maxScoreValue = dictionary["maxScoreValue"] as? Int,
            let updatedAt = dictionary["updatedAt"] as? Date else {
                return nil
        }
        
        self.score = score
        self.minScoreValue = minScoreValue
        self.maxScoreValue = maxScoreValue
        self.updatedAt = updatedAt
    }
}
