import Foundation
import UIKit

struct SegmentedGradientConfiguration {
    var colors: [UIColor] = [.orange, UIColor(hex6: 0xe0c706), UIColor(hex6: 0x2caf15)] // [orange, yellow, green]
    var maxAngle = 360
    var initialAngle = -90
    var defaultColor: UIColor = .black
    var lineWidth: CGFloat = 8
}

extension SegmentedGradientConfiguration: GradientViewConfiguration {
    
    func calculateNumberOfSegments() -> Int {
        guard !colors.isEmpty, colors.count > 1  else {
            return 1
        }
        
        return colors.count - 1
    }
    
    func determineColor(at angle: Int) -> UIColor {
        guard !colors.isEmpty else {
            return defaultColor
        }
        
        guard colors.count > 1 else {
            return colors[0]
        }
        
        // A segment represents a transition between two colors
        let numberOfSegments = calculateNumberOfSegments()
        let segmentSize = maxAngle / numberOfSegments
        
        // Determine start and end color for segment gradient
        let currentSegment = (angle * numberOfSegments) / maxAngle
        let startColor = colors[currentSegment]
        let endColor = colors[currentSegment+1]
        
        // Determine the percentage within that segment
        let percentage = CGFloat((angle - (currentSegment * segmentSize))) * 1.0 / CGFloat(segmentSize)
        
        // Get RBB values for start and end colors
        let startRed = startColor.cgColor.components![0]
        let startGreen = startColor.cgColor.components![1]
        let startBlue = startColor.cgColor.components![2]
        
        let endRed = endColor.cgColor.components![0]
        let endGreen = endColor.cgColor.components![1]
        let endBlue = endColor.cgColor.components![2]
        
        // Determine next color
        let red = startRed + percentage * (endRed - startRed)
        let green = startGreen + percentage * (endGreen - startGreen)
        let blue = startBlue + percentage * (endBlue - startBlue)
        
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
}
