import Foundation
import RxSwift

enum DashboardState {
    case noData
    case loadError(String)
    case loaded
    case refreshing
    case refreshError(String)
}

struct DashboardViewModel {
    
    let api: ClearScoreAPI
    let disposeBag = DisposeBag()
    let viewState = Variable<DashboardState>(.noData)
    let updatedAt = Variable<String>("")
    let maxScore = Variable<Int>(0)
    let creditScore = Variable<Int>(0)
    
    let dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateStyle = .medium
        df.timeStyle = .medium
        
        return df
    }()
    
    init(api: ClearScoreAPI = ClearScoreService()) {
        self.api = api
        
        loadPersistedReport()
    }
    
    func loadPersistedReport() {
        let key = Constants.UserDefaults.creditReport
        let persistedReportDict = UserDefaults.standard.dictionary(forKey: key)
        
        if let dict = persistedReportDict, let report = CreditReport(with: dict) {
            process(creditReport: report)
        }
    }
    
    func persist(creditReport: CreditReport) {
        let key = Constants.UserDefaults.creditReport
        UserDefaults.standard.set(creditReport.asDictionary(), forKey: key)
    }
    
    func refreshCreditScore() {
        setLoadingState()
        
        api.getCreditReport { result in
            switch result {
            case .success(let report):
                self.process(creditReport: report)
            case .failure(let error):
                self.process(error: error)
            }
        }
    }
    
    func setLoadingState() {
        switch viewState.value {
        case .noData:
            return
        case .loadError:
            viewState.value = .noData
        default:
            viewState.value = .refreshing
        }
    }
    
    func process(creditReport: CreditReport) {
        persist(creditReport: creditReport)
        
        maxScore.value = creditReport.maxScoreValue
        creditScore.value = creditReport.score
        updatedAt.value = "Updated at \(dateFormatter.string(from: creditReport.updatedAt))"
        viewState.value = .loaded
    }
    
    func process(error: ClearScoreAPIError) {
        let errorMessage = error.message
        let previousState = viewState.value
        
        switch previousState {
        case .noData, .loadError:
            viewState.value = .loadError(errorMessage)
        case .refreshing:
            viewState.value = .refreshError(errorMessage)
        default:
            break
        }
    }
}

