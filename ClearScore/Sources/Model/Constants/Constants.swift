import Foundation

struct Constants {
    
    struct Xibs {
        static let circularProgressView = "CircularProgressView"
    }
    
    struct UserDefaults {
        static let creditReport = "creditReport"
    }
}
