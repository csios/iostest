import UIKit

extension Int {
    var toRadians: CGFloat { return CGFloat(self) * .pi / 180 }
}
