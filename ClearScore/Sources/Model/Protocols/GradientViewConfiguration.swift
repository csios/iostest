import Foundation
import UIKit

protocol GradientViewConfiguration {
    var maxAngle: Int { get set }
    var initialAngle: Int  { get set }
    var defaultColor: UIColor { get set }
    var lineWidth: CGFloat { get set }
    
    func determineColor(at angle: Int) -> UIColor
}

extension GradientViewConfiguration {
    
    func determineColor(at angle: Int) -> UIColor {
        return defaultColor
    }
}
