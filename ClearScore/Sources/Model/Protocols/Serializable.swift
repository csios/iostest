import Foundation

protocol Serializable {
    func asDictionary() -> [String : Any]
    init?(with dictionary: [String : Any])
}
