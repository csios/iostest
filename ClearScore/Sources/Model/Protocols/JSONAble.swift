import Foundation
import SwiftyJSON

protocol JSONAble {
    init?(json: JSON)
}
