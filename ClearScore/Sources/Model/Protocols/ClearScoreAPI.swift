import Foundation
import Result

protocol ClearScoreAPI {
    func getCreditReport(completion: @escaping (Result<CreditReport, ClearScoreAPIError>) -> Void)
}
