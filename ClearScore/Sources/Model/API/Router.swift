import Foundation

enum Router {
    case creditReport
    
    var url: String {
        switch self {
        case .creditReport:
            return "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/prod/mockcredit/values"
        }
    }
    
    var method: String {
        switch self {
        case .creditReport:
            return "GET"
        }
    }
        
    var urlRequest: URLRequest {
        guard let url = URL(string: url) else {
            fatalError("Invalid URL for \(self)")
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method
        
        return urlRequest
    }
}
