import Foundation

enum ClearScoreAPIError: Error {
    case generalError(Error)
    case parsingFailed
    case noDataRetrieved
    
    var message: String {
        switch self {
        case .generalError(let error):
            return error.localizedDescription
        case .parsingFailed:
            return "Error parsing data"
        case .noDataRetrieved:
            return "No data retrieved"
        }
    }
}

