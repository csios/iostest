import Foundation
import SwiftyJSON
import Result

struct ClearScoreService: ClearScoreAPI {
    
    private func request<T: JSONAble>(endpoint: Router, completion: @escaping (Result<T, ClearScoreAPIError>) -> Void) {
        let session = URLSession(configuration: .default, delegate: .none, delegateQueue: OperationQueue.main)
        
        let dataTask = session.dataTask(with: endpoint.urlRequest) { data, response, error in
            if let e = error {
                completion(.failure(.generalError(e)))
            } else if let d = data {
                let json = JSON(data: d)
                
                if let value = T(json: json) {
                    completion(.success(value))
                } else {
                    completion(.failure(.parsingFailed))
                }
                
            } else {
                completion(.failure(.noDataRetrieved))
            }
        }
        
        dataTask.resume()
    }
    
    func getCreditReport(completion: @escaping (Result<CreditReport, ClearScoreAPIError>) -> Void) {
        let endpoint = Router.creditReport
        request(endpoint: endpoint) { result in
            completion(result)
        }
    }
}
