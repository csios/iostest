import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        guard let rootViewController = window?.rootViewController as? UINavigationController else {
            fatalError("Expecting UINavigationController as root view controller")
        }
        
        guard let dashboardViewController = rootViewController.topViewController as? DashboardViewController else {
            fatalError("Expecting DashboardViewController as first view controller")
        }
        
        dashboardViewController.viewModel = DashboardViewModel()
        
        return true
    }

}
