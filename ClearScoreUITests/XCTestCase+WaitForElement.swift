import XCTest

extension XCTestCase {
    
    func waitFor(_ element: XCUIElement, seconds waitSeconds: Double) {
        let exists = NSPredicate(format: "exists == 1")
        expectation(for: exists, evaluatedWith: element, handler: nil)
        waitForExpectations(timeout: waitSeconds, handler: nil)
    }
    
}
