import XCTest

class ClearScoreUITests: XCTestCase {
    
    let timeout = 5
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        XCUIApplication().launch()
    }
    
    lazy var navigationBar: XCUIElement = {
        let app = XCUIApplication()
        return app.navigationBars["Dashboard"]
    }()
    
    lazy var refreshButton: XCUIElement = {
        let app = XCUIApplication()
        return app.buttons["Refresh"]
    }()
    
    func testLoadCreditScore() {
        // Assert we are on the dashboard screen
        XCTAssertTrue(navigationBar.exists)
        
        // Refresh button will only be there after the API call completes successfuly
        waitFor(refreshButton, seconds: 5)
    }
    
    func testRefreshCreditScore() {
        // Reuse the previous test, which places us on the dashboard screen with the credit score loaded and the refresh button visible
        testLoadCreditScore()
        
        // Tapping refresh button will start a new API call and hide the button
        refreshButton.tap()
        XCTAssertFalse(refreshButton.exists)
        
        // Again, the refresh button will only be there after the API call completes successfuly
        waitFor(refreshButton, seconds: 5)
    }

}
