import XCTest
@testable import ClearScore

class DashboardViewModelTests: XCTestCase {
    
    func testRequestCreditReport() {
        let now = Date()
        let mockReport = CreditReport(score: 123, minScoreValue: 0, maxScoreValue: 456, updatedAt: now)
        let stub = ClearScoreServiceStub(mockReport: mockReport)
        
        let viewModel = DashboardViewModel(api: stub)
        viewModel.refreshCreditScore()
        
        XCTAssertEqual(viewModel.creditScore.value, 123)
        XCTAssertEqual(viewModel.maxScore.value, 456)
        XCTAssertEqual(viewModel.updatedAt.value, "Updated at \(viewModel.dateFormatter.string(from: now))")
    }
}
