import Foundation
import Result
@testable import ClearScore

struct ClearScoreServiceStub: ClearScoreAPI {
    
    let mockReport: CreditReport
    
    func getCreditReport(completion: @escaping (Result<CreditReport, ClearScoreAPIError>) -> Void) {
        completion(.success(mockReport))
    }
}
