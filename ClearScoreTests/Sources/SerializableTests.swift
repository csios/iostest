import XCTest
@testable import ClearScore

class SerializableTests: XCTestCase {
    
    func testCreditReportSerialization() {
        
        let dict: [String : Any] = [
            "score": 10,
            "minScoreValue": 0,
            "maxScoreValue": 700,
            "updatedAt": Date()
        ]
        
        guard let creditReport = CreditReport(with: dict) else {
            XCTFail("Error deserializing credit report")
            return
        }
        
        // Check if the resulting dictionary matches the dictionary used to create the object
        let resultingDict = creditReport.asDictionary()
        XCTAssert(NSDictionary(dictionary: resultingDict).isEqual(to: dict))
    }
}
