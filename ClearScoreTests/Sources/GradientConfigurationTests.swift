import XCTest
@testable import ClearScore

class GradientConfigurationTests: XCTestCase {
    
    func testConfigurationWithEmptyColorsUsesDefaultColor() {
        
        var configuration = SegmentedGradientConfiguration()
        configuration.colors = []
        configuration.maxAngle = 50
        
        (0..<configuration.maxAngle).forEach { angle in
            let color = configuration.determineColor(at: angle)
            XCTAssertEqual(color, configuration.defaultColor)
        }
    }
    
    func testConfigurationWithOneColorsUsesThatColor() {
        let customColor = UIColor.red
        
        var configuration = SegmentedGradientConfiguration()
        configuration.colors = [customColor]
        configuration.maxAngle = 50
        
        (0..<configuration.maxAngle).forEach { angle in
            let color = configuration.determineColor(at: angle)
            XCTAssertEqual(color, customColor)
        }
    }
    
    func testConfigurationWithoutColorsHasOneSegment() {
        var configuration = SegmentedGradientConfiguration()
        configuration.colors = []
        
        XCTAssertEqual(configuration.calculateNumberOfSegments(), 1)
    }
    
    func testConfigurationWithOneColorHasOneSegment() {
        var configuration = SegmentedGradientConfiguration()
        configuration.colors = [UIColor.red]
        
        XCTAssertEqual(configuration.calculateNumberOfSegments(), 1)
    }
    
    func testConfigurationWithTwoColorsHasOneSegment() {
        var configuration = SegmentedGradientConfiguration()
        configuration.colors = [UIColor.red, UIColor.green]
        
        XCTAssertEqual(configuration.calculateNumberOfSegments(), 1)
    }
    
    func testConfigurationWithThreeColorsHasTwoSegments() {
        var configuration = SegmentedGradientConfiguration()
        configuration.colors = [UIColor.red, UIColor.green, UIColor.blue]
        
        XCTAssertEqual(configuration.calculateNumberOfSegments(), 2)
    }
}
