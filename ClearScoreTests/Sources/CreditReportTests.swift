import XCTest
import SwiftyJSON
@testable import ClearScore

class CreditReportTests: XCTestCase {
    
    var creditReportResponse: JSON {
        @objc class TestClass: NSObject { }
        
        let bundle = Bundle(for: TestClass.self)
        let path = bundle.path(forResource: "creditReport", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        
        return JSON(data: data)
    }
    
    func testJSONInitialization() {
        guard let report = CreditReport(json: creditReportResponse) else {
            XCTFail("Failed to load credit report sample response")
            return
        }
        
        XCTAssertEqual(report.score, 514)
        XCTAssertEqual(report.minScoreValue, 0)
        XCTAssertEqual(report.maxScoreValue, 700)
    }    
}
